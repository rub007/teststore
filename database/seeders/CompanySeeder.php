<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $companyUser = User::create([
            'name' => 'google',
            'email' => 'google@teststore.com',
            'password' => Hash::make('11111111'),
            'role_id' => Role::where('title',Role::COMPANY)->first()->id
        ]);

        Company::create([
            'user_id' => $companyUser->id,
            'name' => 'google',
            'website_url' => 'https://google.com'
        ]);
    }
}
