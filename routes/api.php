<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('/login', [\App\Http\Controllers\AuthController::class,'login'])->name('login');

Route::group(['middleware' => 'auth:api'],function (){

    Route::group(['prefix' => 'applications'],function (){
        Route::get('/',[\App\Http\Controllers\ApplicationController::class,'index']);
        Route::get('/{application}',[\App\Http\Controllers\ApplicationController::class,'view']);
        Route::post('/',[\App\Http\Controllers\ApplicationController::class,'create']);
        Route::delete('/{application}',[\App\Http\Controllers\ApplicationController::class,'delete']);
    });


    Route::group(['prefix' => 'purchases'],function(){
        Route::get('/',[\App\Http\Controllers\PurchaseController::class,'index']);
        Route::post('/',[\App\Http\Controllers\PurchaseController::class,'create']);
        Route::delete('/{purchase}',[\App\Http\Controllers\PurchaseController::class,'delete']);
        Route::get('/company-purchases-amount',[\App\Http\Controllers\PurchaseController::class,'getCompanyAllPurchasesAmount']);
        Route::get('/store-purchases-amount',[\App\Http\Controllers\PurchaseController::class,'getStoreAllPurchasesAmount']);
    });

});

