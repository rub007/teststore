<?php

namespace App\Providers;

use App\Models\Application;
use App\Models\Purchase;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
         'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        if (! $this->app->routesAreCached()) {
            Passport::routes();
        }

        Gate::define('delete-application', function (User $user,Application $application) {
            return (Auth::user()->isRole(Role::COMPANY) && Auth::user()->company->id === $application->company_id);
        });

        Gate::define('delete-purchase', function (User $user,Purchase $purchase) {
            return (Auth::user()->isRole(Role::REGULAR) && Auth::id() === $purchase->user_id);
        });
    }
}
