<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Application extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable = [
        'title',
        'icon_url',
        'brief_description',
        'description',
        'price',
        'company_id'
    ];

    public $isInstalled;

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function getCommissionPercentAttribute()
    {
        return Purchase::SHOP_COMMISSION_PERCENT;
    }

    public function purchases()
    {
        return $this->hasMany(Purchase::class);
    }
}
