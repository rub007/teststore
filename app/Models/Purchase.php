<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Purchase extends Model
{
    use HasFactory,SoftDeletes;

    public const SHOP_COMMISSION_PERCENT = 40;

//    Note: purchase_price & commission_percent columns are not data duplicate,
//          this columns prevent situation when changing SHOP_COMMISSION_PERCENT
//          can cause incorrect calculations

    protected $fillable = [
        'user_id',
        'application_id',
        'purchase_price',
        'commission_percent'
    ];

    public static function checkIfAlreadyPurchased($application_id,$user_id)
    {
        return (bool)
            self::where('application_id', $application_id)
                ->where('user_id',$user_id)
                ->first();
    }

    public static function getCompanyAllPurchasesSum($userId)
    {
        return DB::table('purchases')
            ->select(DB::raw('SUM(purchase_price - purchase_price*commission_percent/100) as `sum`'))
            ->join('applications','applications.id','=','purchases.application_id')
            ->join('companies','companies.id','=','applications.company_id')
            ->where('companies.user_id',$userId)
            ->first();
    }

    public static function getStoreAllPurchasesSum()
    {
        return DB::table('purchases')
            ->select(DB::raw('SUM(purchase_price*commission_percent/100) as `sum`'))
            ->first();
    }
}
