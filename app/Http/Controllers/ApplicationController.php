<?php

namespace App\Http\Controllers;


use App\Http\Requests\CreateApplicationRequest;
use App\Http\Resources\ApplicationCollection;
use App\Http\Resources\ApplicationResource;
use App\Models\Application;
use App\Models\Purchase;
use App\Models\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class ApplicationController extends Controller
{
    public function index()
    {
        $installedApplicationIds = Purchase::where('user_id',Auth::id())->pluck('application_id')->toArray();
        $applications = Application::with('company')->get();
        foreach ($applications as $application)
        {
            $application->isInstalled = in_array($application->id,$installedApplicationIds);
        }
        $response = new ApplicationCollection(ApplicationResource::collection($applications));

        return response()->json([$response]);
    }

    public function view(Application $application)
    {
        if (Auth::user()->isRole(Role::REGULAR)){
            return response()->json($application);
        }
        return abort(403,'No permissions');
    }

    public function create(CreateApplicationRequest $request): \Illuminate\Http\JsonResponse
    {
        $application = Application::create([
            'title' => $request->title,
            'icon_url' => $request->icon_url,
            'brief_description' => $request->brief_description,
            'description' => $request->description,
            'price' => $request->price ?? null,
            'company_id' => Auth::user()->company->id,
        ]);

        return response()->json(['application' => $application]);
    }

    public function delete(Application $application): \Illuminate\Http\JsonResponse
    {
        if (Gate::allows('delete-application',$application)){
            $application->delete();
            return response()->json(['success' => true],202);
        }

        return abort(403,'No permissions');
    }
}
