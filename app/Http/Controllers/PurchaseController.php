<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePurchaseRequest;
use App\Models\Application;
use App\Models\Purchase;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class PurchaseController extends Controller
{
    public function index()
    {
        if (Auth::user()->isRole(Role::REGULAR)){
            $applications = Application::whereHas('purchases',function ($query){
                $query->where('user_id',Auth::id());
            })->get();

            $spentMoney = Auth::user()->purchases()->sum('purchase_price');
            return response()->json(['applications' => $applications,'total_spent' => $spentMoney]);
        }
        return abort(403,'No Permissions');
    }

    public function create(CreatePurchaseRequest $request)
    {
        $application = Application::find($request->application_id);
        Purchase::create([
            'user_id' => Auth::id(),
            'application_id' => $application->id,
            'purchase_price' => $application->price,
            'commission_percent' => $application->commission_percent,
        ]);
        return response()->json(['success' => true]);
    }

    public function delete(Purchase $purchase)
    {
        if (Gate::allows('delete-purchase',$purchase)){
            $purchase->delete();
            return response()->json(['success' => true],202);
        }
        return abort(403,'No permissions');
    }

    public function getCompanyAllPurchasesAmount()
    {
        if (Auth::user()->isRole(Role::COMPANY)){
            $sum = Purchase::getCompanyAllPurchasesSum(Auth::id())->sum;
            return response()->json(['sum' => $sum]);
        }
        return abort(403,'No Permissions');
    }

    public function getStoreAllPurchasesAmount()
    {
        if (Auth::user()->isRole(Role::ADMIN)){
            $sum = Purchase::getStoreAllPurchasesSum()->sum;
            return response()->json(['sum' => $sum]);
        }
        return abort(403,'No Permissions');
    }
}
