<?php

namespace App\Http\Requests;

use App\Models\Application;
use App\Models\Purchase;
use App\Models\Role;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CreatePurchaseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return (Auth::user()->isRole(Role::REGULAR) &&
                !Purchase::checkIfAlreadyPurchased($this->application_id,Auth::id()));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'application_id' => ['required','exists:applications,id']
        ];
    }
}
