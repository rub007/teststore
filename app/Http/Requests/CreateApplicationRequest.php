<?php

namespace App\Http\Requests;

use App\Models\Role;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CreateApplicationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::user()->isRole(Role::COMPANY);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'title' => ['required','string'],
            'icon_url' => ['required','string'],
            'brief_description' => ['required','max:255'],
            'description' => ['required','max:8000'],
            'price' => ['bail','numeric','max:100000']
        ];
    }
}
